* Matos
** Contrôleur
** Laser   
* Installation
** Pilotes arduino
   Pas certains que les pilotes arduino soient absolument nécessaires mais vaut mieux le faire. Sous
   ubuntu dans un terminal :
   #+begin_example
   sudo apt install arduino
   #+end_example
** UGSplatform
   Permet de contrôler la graveuse laser. Téléchargeable ici :
   https://winder.github.io/ugs_website/download/ \\
   Prendre la version classique ou platform, l'enregistrer dans un dossier au choix et déziper
   l'archive .zip. \\
   Dans un terminal :
   - Aller dans ce dossier. \\ 
   #+begin_example
   cd Dossier/ou/est/dezippé/l'archive
   #+end_example
   - Lancer le logiciel :
   Avec la version classique : \\
   #+begin_example
   java -jar UniversalGcodeSender.jar
   #+end_example
   Avec la version platform :
   #+begin_example
   ./bin/ugsplatform
   #+end_example
   Si vous avez des problèmes par la suite pour contrôler la graveuse, il est possible que le
   logiciel n'ait pas les droits pour accéder à certains fichiers, dans ce cas 2 possibilités : \\
   - Version propre : trouver les fichiers en accés protégé et modifier les permissions.
   - Version sale : lancer le logiciel en super utilisateur en ajoutant "sudo" à la commande précédente.
* Paramétrage
  Commencer par brancher la graveuse en usb. Le moteur devrait faire un petit bruit satisfaisant
  signifiant qu'elle est reconnue.
** Ports
   Dans UGS : le port devrait être reconnu automatiquement. Pour moi (sous xubuntu 18.04), c'était
   usbtty0. \\
** Baud
   Choisir un Baud de 115200.
** Firmware
   Choisir GRBL
** Connexion
   Cliquer sur "Open", vous devriez pouvoir contrôler la graveuse. Si ce n'est pas le cas essayez de
   lancer le logiciel avec sudo, ou encore d'installer la dernière version des pilotes arduino
** Paramétrage du nombre de pas par mm
   Par défaut, ugsplatform fixe 250 pas du moteur pour déplacer le laser d'un millimètre. Ca ne
   correspond pas à notre matos et donc tout est surdimensionné. Pour l'instant, j'ai paramétré à
   l'arrache en dessinant un trait de 10cm, je l'ai mesuré (31.5cm) et j'ai divisé le nombre de pas
   par le rapport des deux (3.15), on arrive donc à 79.3 pas par mm. C'est pas mal mais pour des
   applications qui demandent vraiment de la précision c'est pas fou. Il faudrait suivre ce tuto
   pour le faire proprement : https://diymachining.com/grbl-steps-per-mm/ \\
   Dans la console d'UGS, on peut taper $$ pour connaître les paramètres actuels. \\
   On peut redéfinir le nombre de pas par mm en tapant dans la console d'UGS $100 = xxx et $101 = yyy avec xxx et yyy les
   valeurs choisies. $100 contrôle le nombre de pas par mm sur l'axe X et $101 sur l'axe Y.
** Réglage de la focale
Pour que le tracé soit net, il faut régler la focale. Placer votre support sous le laser, allumer le laser en faible intensité, en entrant M03 G1 F200 S100 dans la console. Un point apparaît. Régler la molette de la focale sur le laser pour obtenir un point bien net.
* Dessin et export en Gcode
  On peut décider d'écrire directement le code en Gcode, mais c'est relou. On peut plutôt dessiner
  le motif à graver avec un logiciel de dessin vectoriel, par exemple inkscape.
** Dessin sous inkscape
   Se référer à un bon tuto sur l'utilisation d'inkscape. https://inkscape.org/fr/learn/tutorials/
** Préparation du dessin
   L'ensemble de votre dessin doit être sous forme de chemin. Si vous avez utilisé les outils de
   dessin de formes ou de texte par exemple ça ne sera pas le cas. Sélectionner la forme puis
   Chemins -> Objet en chemin (ou Maj + Ctrl + C) \\
   Il est possible de partir d'une image en la vectorisant :
   https://inkscape.org/fr/doc/tutorials/tracing/tutorial-tracing.fr.html \\
   Si votre dessin comporte plusieurs parties, il faut les grouper : toutes les sélectionner puis
   Objets -> grouper (ou Ctrl+a puis Ctrl+g) \\
** Conversion en Gcode
*** Installation de l'utilitaire
    Il y a un utilitaire de conversion en Gcode inclus dans les dernières versions d'Inkscape mais il
    n'est pas simple d'utilisation. On utilise JTech Photonics Laser Tool :
    https://jtechphotonics.com/?page_id=2012 \\
    Installer l'extension dans inkscape : https://inkscape.org/gallery/=extension/ \\
*** Conversion et paramétrage
    Une fois le dessin converti en chemin, groupé et sélectionner, lancer JTech Photonics Laster
    Tool depuis le menu Extensions d'inkscape. \\
    - Laser ON Commande : M03
    - Laser OFF Command : M05
    - Travel Speed : C'est la vitesse de déplacement du laser quand il est éteint. Elle peut être
      assez rapide, mais pas trop pour éviter de l'abimer. Je l'ai mise à 1500 mm/min
    - Laser Speed : C'est la vitesse de déplacement quand le laser est éteint. A tester et régler en
      fonction du type de bois, en fonction aussi de la puissance. Je l'avais mise à 300 mm/min
    - Laser Power : entre 0 et 12000, pour un bois blanc et un trait bien marqué je l'avais mise
      à 10000.
    - Laisser Power On Delay à 0, Passes à 1 et Pass Depth à 0.5
    - Définir le dossier où le fichier en .gcode sera enregistré
    - Choisir le nom du fichier
    - Bien définir les unités en mm 
* Lancement  
** Chargement du fichier
   Dans UGS, ouvrir le fichier en .gcode sorti par inkscape.
** Définition du zéro
   Il faut ensuite définir le point de référence des axes pour UGS. Pour cela déplacer le laser avec
   UGS au point voulu puis cliquer sur "Reset Zero". On peut se repérer dans l'onglet visualisation, le cône jaune
   indique l'emplacement du zéro.
** Lancement du script
   Cliquer sur "Run". C'est parti.

Bisous
VonHammer
